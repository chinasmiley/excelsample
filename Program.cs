using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Configuration;

namespace SimpleExcel
{
    public class Program
    {
        //Load output excel file folder from app.config
        static string ExcelFilePath = System.Configuration.ConfigurationManager.AppSettings["FilePath"] + "CSharpExcel_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";
        static int RowNum;
        static int ColumnNum;

        public static void Main(string[] args)
        {
            Log log = new Log();
            try
            {
                
                Console.WriteLine("Enter command");
                string line = Console.ReadLine();
                string[] Operations;
                ExcelObj myExcel = new ExcelObj();
                
                //read command from console
                while (line.Trim().ToUpper() != "Q")
                {
                    if (line.Trim().Length == 0)
                    {
                        Console.WriteLine("Please key in command");
                        log.WriteLog("Excel is not properly installed!!");
                    }
                    //call method for different command
                    Operations = line.Split(' ');
                    switch (Operations[0].ToUpper())
                    {
                        //create the excel
                        case "C":
                            //validate the parameters
                            if (Operations.Length != 3) 
                            {
                                Console.WriteLine("Please create a new spread sheet with three parameters, C, width w and height h");
                                log.WriteLog("Command C indicate wrong number of parameters " + line);
                                break;
                            }
                            ColumnNum = int.Parse(Operations[1]);
                            RowNum = int.Parse(Operations[2]);

                            myExcel.CreateExcel(ExcelFilePath, ColumnNum, RowNum);
                            myExcel.PrintExcel(ColumnNum, RowNum);

                            break;
                        //insert the records into excel
                        case "N":
                            //validate the parameters
                            if (Operations.Length != 4)
                            {
                                Console.WriteLine("Please insert records with four parameters, N, x, y, value");
                                log.WriteLog("Command N indicate wrong number of parameters " + line);
                                break;
                            }
                            myExcel.InsertExcel(int.Parse(Operations[1]), int.Parse(Operations[2]), Operations[3]);
                            myExcel.PrintExcel(ColumnNum, RowNum);

                            break;
                        case "S": //calculate the sum of the range from (y1, x1) to (y2, x2) and save the value into (y3, x3)
                            if (Operations.Length != 7)//validate parameters
                            {
                                Console.WriteLine("Please calculate records with seven parameters, S x1 y1 x2 y2 x3 y3");
                                log.WriteLog("Command S indicate wrong number of parameters " + line);
                                break;
                            }
                            myExcel.SumRange(int.Parse(Operations[1]), int.Parse(Operations[2]), int.Parse(Operations[3]), int.Parse(Operations[4]), int.Parse(Operations[5]), int.Parse(Operations[6]));
                            myExcel.PrintExcel(ColumnNum, RowNum);
                            break;
                        
                        default:
                            break;

                    }

                    //read next command
                    line = Console.ReadLine();

                }
                //Q command to quit the program
                if (ColumnNum > 0 && RowNum > 0)
                    myExcel.PrintExcel(ColumnNum, RowNum);
                if (myExcel != null)
                    myExcel.QuitExcel();
                return;
            }
            catch(Exception e)
            {
                log.WriteLog(e.Message);
                throw;
            }
            

        }
               

        
    }
    public class Log
    {
        //Load log file folder from app.config
        private static string LogFolder =  System.Configuration.ConfigurationManager.AppSettings["LogFolder"] + "Log_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
        public void WriteLog(string Message)
        {
            System.IO.StreamWriter oWrite = default(System.IO.StreamWriter);
            oWrite = System.IO.File.AppendText(LogFolder);
            oWrite.WriteLine(DateTime.Now.ToString() + ": " + Message);
            oWrite.Close();
        }


    }
}
