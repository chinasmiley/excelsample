using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace SimpleExcel
{
    public class ExcelObj
    {
        private SimpleExcel.Log log;
        private string ExcelPath;
        private static Excel.Application xlApp;
        private static Excel.Workbook wkb;
        private static Excel.Worksheet wks;
        object misValue = System.Reflection.Missing.Value;

        public ExcelObj()
        {
            this.log = new Log();

        }
        /************************************************** 
         * This function print the excel content in console
         **************************************************/
        public void PrintExcel(int column, int row)
        {
            string cellvalue = "";
            log.WriteLog("Browse Excel " + ExcelPath);
            string line;
            string header = "-";
            //loop from row 1
            for (int i = 1; i <= row; i++)
            {
                line = "|";
                // loop cell from column 1 to the last
                for (int j = 1; j <= column; j++)
                {
                    if(i == 1)
                        header += "--";
                    // check whether cell is blank
                    if ( wks.Cells[i, j].text != "") 
                        cellvalue = wks.Cells[i, j].Value.ToString();
                    else
                        cellvalue = " ";
                    
                    line = line + cellvalue + " ";
        }
                line += "|";
                if( i == 1)
                    Console.WriteLine(header);
                Console.WriteLine(line);
                log.WriteLog(line);
            }
            Console.WriteLine(header);


        }
        /***************************************************
         * create Excel file
         ***************************************************/
        public void CreateExcel(string filepath, int column, int row)
        {
            try
            {
                xlApp = new Microsoft.Office.Interop.Excel.Application();
                ExcelPath = filepath;
                if (File.Exists(ExcelPath))
                {
                    File.Delete(ExcelPath);
                }
                

                log.WriteLog("Create Excel "+ column.ToString() + "colum " + row.ToString() +" row on " + filepath.ToString());
                object misValue = System.Reflection.Missing.Value;

                wkb = xlApp.Workbooks.Add(misValue);
                wks = (Excel.Worksheet)wkb.Worksheets.get_Item(1);
                int i = 1;
                int j = 1;
                for (i = 1; i <= column; i++)
                {
                    for (j = 1; j <= row; j++)
                    {
                        wks.Cells[j, i] = "";
                    }

                }

                // save excel file
                wkb.SaveAs(filepath, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                log.WriteLog("File is saved " + filepath.ToString());

            }
            catch(Exception e)
            {
                log.WriteLog(e.Message);
                return ;
            }

        }
        /***************************************************
         * insert record into Excel file
         ***************************************************/
        public void InsertExcel(int x, int y, string value)
        {
            log.WriteLog("Insert " + x.ToString() + " " + y.ToString() + " " + value.ToString() + " to " + ExcelPath);
            wks.Cells[y, x] = value;
            wkb.Save();
        }

        /**************************************************************************************
         * Calculate sum of the range from (y1,x1) to (y2,x2) and save the value to (y3, x3)
         **************************************************************************************/
        public void SumRange (int x1, int y1, int x2, int y2,int x3, int y3)
        {
            int sum=0;
            string cellvalue = "";
            int temp;
            log.WriteLog("Sum up Excel from (" + y1.ToString() +","+ x1.ToString() + ") to (" + y2.ToString() + ","+x2.ToString() +") sum value in ("+ y3.ToString() +","+ x3.ToString()+ ") ");
            //set (y1, x1) to be the top left cell if it is not, (y2, x2) shall be the bottom right cell
            if ( x1 > x2 ) 
            {
                temp = x1;
                x1 = x2;
                x2 = temp;
            }
            if (y1 > y2)
            {
                temp = y1;
                y1 = y2;
                y2 = temp;
            }
            for (int r = y1; r <= y2; r++) //loop row
            {
                for (int c = x1; c <= x2; c++) // loop each cell in one row
                {
                    temp = 0;
                    if (wks.Cells[r, c].text != "") 
                    {
                        cellvalue = wks.Cells[r, c].Value.ToString();
                        if (cellvalue != "")
                            temp = int.Parse(cellvalue);
                        sum += temp; 
                    }
                    else
                        continue;
                }
            }
            wks.Cells[y3, x3] = sum;
            wkb.Save();
            log.WriteLog("S operation is complete");

        }
        /***************************************************
         * retrieve cell value 
         ***************************************************/
        public int GetExcelValue(int column, int row)
        {
            
            int temp=0;
            try
            {

                if (wks.Cells[row, column].text != "")
                    temp = int.Parse(wks.Cells[row, column].Value.ToString());
                else
                    temp = 0;
                return temp;
               
            } catch (Exception e)
            {
                log.WriteLog(e.Message);
                throw;
            }


        }
        /***************************************************
         * QuitExcel record into Excel file
         ***************************************************/
        public void QuitExcel()
        {
            log.WriteLog("Q operation starts");
            if (wkb != null)
            {
                wkb.Save();
                wkb.Close(0); 
                xlApp.UserControl = true;
                xlApp.Quit();
            }
            ReleaseComObject(wks);
            ReleaseComObject(wkb);
            ReleaseComObject(xlApp);
            log.WriteLog("Q operation is complete");
        }
        private void ReleaseComObject(object obj)
        {
            try
            {
                Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception)
            {
                obj = null;
            }
        }
    }


}
