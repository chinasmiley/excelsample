using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleExcel;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleExcel.Tests
{
    [TestClass()]
    public class ExcelObjTests
    {
        string filepath = "c:\\test\\testexcel.xls";
        [TestMethod()]
        public void CreateExcelTest()
        {
            
            SimpleExcel.ExcelObj myExcel = new SimpleExcel.ExcelObj();
            myExcel.CreateExcel(filepath, 4, 4);
            myExcel.QuitExcel();
            Assert.IsTrue(File.Exists(filepath));
        }

        [TestMethod()]
        public void CreateExcelExceptionTest()
        {
            Exception expectedExcetpion = null;
            try
            {
                SimpleExcel.ExcelObj myExcel = new SimpleExcel.ExcelObj();
                myExcel.CreateExcel("d:\\", 4, 4);
                myExcel.QuitExcel();
                //Assert.Fail();

            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is AssertFailedException);
            }
            
            
        }

        [TestMethod()]
        public void InsertExcelTest()
        {
            
            SimpleExcel.ExcelObj myExcel = new SimpleExcel.ExcelObj();
            myExcel.CreateExcel(filepath, 4, 4);
            myExcel.InsertExcel(1, 1, "1");

            Assert.AreEqual(myExcel.GetExcelValue(1, 1), 1);
            myExcel.QuitExcel();


        }

        [TestMethod()]
        public void SumRangeTest()
        {
            
            SimpleExcel.ExcelObj myExcel = new SimpleExcel.ExcelObj();
            myExcel.CreateExcel(filepath, 4, 4);
            myExcel.InsertExcel(1, 1, "1");
            myExcel.InsertExcel(1, 2, "1");
            myExcel.SumRange(1, 1, 1, 2, 1, 3);
            Assert.AreEqual(myExcel.GetExcelValue(2, 1), 0);
            Assert.AreEqual(myExcel.GetExcelValue(1, 3), 2);
            myExcel.QuitExcel();

        }

        [TestMethod()]
        public void SumRangeSecondTest()
        {
            SimpleExcel.ExcelObj myExcel = new SimpleExcel.ExcelObj();
            myExcel.CreateExcel(filepath, 4, 4);
            myExcel.InsertExcel(1, 1, "1");
            myExcel.InsertExcel(2, 2, "1");
            myExcel.SumRange(2, 2, 1, 1, 1, 3);
            Assert.AreEqual(myExcel.GetExcelValue(1, 3), 2);
            myExcel.QuitExcel();

        }

        [TestMethod()]
        public void GetExcelValueTest()
        {
            
            SimpleExcel.ExcelObj myExcel = new SimpleExcel.ExcelObj();
            myExcel.CreateExcel(filepath, 4, 4);
            myExcel.InsertExcel(1, 1, "1");
            Assert.AreEqual(myExcel.GetExcelValue(1, 1), 1);
            myExcel.QuitExcel();
        }

        [TestMethod()]
        public void GetExcelValueExceptionTest()
        {
            SimpleExcel.ExcelObj myExcel = new SimpleExcel.ExcelObj();
            Exception expectedExcetpion = null;
            try
            {
                myExcel.CreateExcel(filepath, 4, 4);
                myExcel.InsertExcel(1, 1, "1");
                myExcel.GetExcelValue(-1, -1);
                

            }
            catch (Exception ex)
            {
                expectedExcetpion = ex;
            }
            Assert.IsNotNull(expectedExcetpion);
        }

        [TestMethod()]
        public void QuitExcelTest()
        {
            SimpleExcel.ExcelObj myExcel = new SimpleExcel.ExcelObj();
            myExcel.CreateExcel(filepath, 4, 4);
            myExcel.QuitExcel();
            Assert.IsTrue(true);

        }
        [TestMethod()]
        public void PrintExcelTest()
        {
            SimpleExcel.ExcelObj myExcel = new SimpleExcel.ExcelObj();
            myExcel.CreateExcel(filepath, 4, 4);
            myExcel.InsertExcel(1, 1, "1");
            myExcel.PrintExcel(4, 4);
            myExcel.QuitExcel();
            Assert.IsTrue(true);

        }

     
    }
}